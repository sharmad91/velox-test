**
 *  The following trading algorithm (algo)  is notified of estimated fair prices and the market prices for each symbol.
 *  Whenever the market price of a symbol is away from its fair price estimate, the symbol is believed to be
 *  undervalued or overvalued in the market. The algo trades the symbol accordingly to make a profit on the
 *  assumption that market price will eventually converge to the fair price estimate. If market price matches
 *  the fair price estimate no trade is necessary.
 *
 *  The algo is given an initial cash balance to start with. Each trade is settled the moment it succeeds
 *  i.e. whenever a fill event is received, cash/shares are available immediately. It is strictly forbidden for the
 *  algo to produce a scenario where its cash balance can become negative, regardless of market conditions.
 *
 *  With the above constraints in place, the algo should produce maximum total asset value (cash balance + market
 *  value of holdings) when the market behaves as expected and moves towards fair price estimate.
 *
 *  Implement the algo by implementing the interface below. Each method represents a callback from an algo
 *  engine. The algo engine guarantees all code to be running on a single thread.  You can create member
 *  variables/methods as necessary in your implementation but can't change the signature of the interface provided. You
 *  must trade (by returning the appropriate value) on each handle method if a profitable trade is possible as long
 *  as of the constraints above are not violated.
 *
 *  Additional information
 * - Upon the call to each handle method, if a valid side is returned the engine creates an order with
 *    - quantity 1
 *    - side as per the returned value
 *    - limit price set at the last known market price
 *    - symbol same as the callback
 * - Side.INVALID indicates no trade desired
 * - An order placed in the market may
 *    - be traded (at or more favorable price compared to the limit price)
 *    - or rejected.
 * - Can't receive fills for more shares than orders placed
 * - None of the prices can be negative.
 *
 * Example 1:
 * setInitialCashBalance(5)
 * notifyEstimatedFairPrice("A", 3);
 * handleMarketPrice("A", 2); (should return BUY)
 * notifyReject(“A”, 1, 2);
 * handleMarketPrice("A", 2); (should return BUY)
 * handleMarketPrice("A", 2); (should return BUY)
 *
 * Example 2:
 * setInitialCashBalance(5)
 * notifyEstimatedFairPrice("A", 3);
 * handleMarketPrice("A", 4); (should return INVALID)
 */
interface QuoteAlgo {
    /**
     *
     * @param initialCashBalance the cash balance to start with
     */
    setInitialCashBalance(initialCashBalance: number): void;

    /**
     * Method called to notify you about the estimated fair price change for a given
     * symbol
     *
     * @param symbol the target symbol
     * @param price  the new/current estimated fair price
     *
     */
    notifyEstimatedFairPrice(symbol: string, price: number): void;

    /**
     * Method called to notify you about the market price change for a given symbol
     *
     * @param symbol the target symbol
     * @param price  the last traded market price (not necessarily our order)
     *
     * @returns the side where an order will be created, or Side.None if no order is
     *          needed. If an order is to be created, it will always be 1 share, for
     *          the given symbol, and on the returned side.
     *
     */
    handleMarketPrice(symbol: string, price: number): Side;

    /**
     * Method called to notify you about a successful trade.
     *
     * @param symbol             the symbol of the fill
     * @param qty                the shares of the fill. Negative value indicates
     *                           sell, positive value indicates buy
     * @param originalLimitPrice the limit price at which you had created the
     *                           order(s)
     * @param fillPrice          the price at which we received the fill
     *
     * @returns the side where an order will be created, or Side.None if no order is
     *          needed. If an order is to be created, it will always be 1 share, for
     *          the given symbol, and on the returned side.
     *
     */
    handleFill(symbol: string, qty: number, originalLimitPrice: number, fillPrice: number): Side;

    /**
     * Method called to notify you that an order is rejected - i.e. you will no
     * longer receive any fill for this order.
     *
     * @param symbol             the symbol of the order
     * @param qty                the qty outstanding that was rejected by the
     *                           exchange
     * @param originalLimitPrice the limit price at which we had created the order
     */
    notifyReject(symbol: string, qty: number, originalLimitPrice: number): void;
}

enum Side {
    Buy,
    Sell,
    Invalid,
}



