import { QuoteAlgo } from "./index";

let obj = new QuoteAlgo();
obj.setInitialCashBalance(5);
obj.notifyEstimatedFairPrice("A", 3);
obj.handleMarketPrice("A", 1);
obj.handleFill("A", 1, 1, 1);
obj.handleMarketPrice("A", 2);
obj.handleMarketPrice("A", 3);
obj.handleFill("A", 1, 1, 1);
obj.handleFill("A", 1, 2, 2);
obj.handleMarketPrice("A", 1);


console.log(obj.getAvailableCash(), obj.getSymbolSnapShot("A"))