export enum Side {
  Buy,
  Sell,
  Invalid,
}

interface IQuoteAlgo {
  /**
   * @param initialCashBalance the cash balance to start with
   */
  setInitialCashBalance(initialCashBalance: number): void;
  /**
   * Method called to notify you about the estimated fair price change for a given
   * symbol
   *
   * @param symbol the target symbol
   * @param price  the new/current estimated fair price
   *
   */

  notifyEstimatedFairPrice(symbol: string, price: number): void;
  /**
   * Method called to notify you about the market price change for a given symbol
   *
   * @param symbol the target symbol
   * @param price  the last traded market price (not necessarily our order)
   *
   * @returns the side where an order will be created, or Side.None if no order is
   *          needed. If an order is to be created, it will always be 1 share, for
   *          the given symbol, and on the returned side.
   *
   */
  handleMarketPrice(symbol: string, price: number): Side;

  /**
   * Method called to notify you about a successful trade.
   *
   * @param symbol             the symbol of the fill
   * @param qty                the shares of the fill. Negative value indicates
   *                           sell, positive value indicates buy
   * @param originalLimitPrice the limit price at which you had created the
   *                           order(s)
   * @param fillPrice          the price at which we received the fill
   *
   * @returns the side where an order will be created, or Side.None if no order is
   *          needed. If an order is to be created, it will always be 1 share, for
   *          the given symbol, and on the returned side.
   *
   */
  handleFill(
    symbol: string,
    qty: number,
    originalLimitPrice: number,
    fillPrice: number
  ): Side;

  /**
   * Method called to notify you that an order is rejected - i.e. you will no
   * longer receive any fill for this order.
   *
   * @param symbol             the symbol of the order
   * @param qty                the qty outstanding that was rejected by the
   *                           exchange
   * @param originalLimitPrice the limit price at which we had created the order
   */
  notifyReject(symbol: string, qty: number, originalLimitPrice: number): void;
}

export class QuoteAlgo implements IQuoteAlgo {
  private availableAmount: number;
  private symbolSnapshot: Record<
    string,
    {
      marketPrice?: number;
      estimatedFairPrice?: number;
      purchasedUnits: number;
      pendingOrders: Record<string, Record<number, number>>;
    }
  >;
  constructor() {
    this.availableAmount = 0;
    this.symbolSnapshot = {};
  }

  initialiseSymbol(symbol: string) {
    if (this.symbolSnapshot[symbol] === undefined) {
      this.symbolSnapshot[symbol] = {
        purchasedUnits: 0,
        pendingOrders: {
          [Side.Buy]: {},
          [Side.Sell]: {},
        },
      };
    }
  }

  setInitialCashBalance(initialCashBalance: number) {
    if (initialCashBalance < 0) {
      throw new Error(
        `Cannot initiate with negative cash balance. Provided value - ${initialCashBalance}`
      );
    }
    this.availableAmount = initialCashBalance;
  }

  notifyEstimatedFairPrice(symbol: string, price: number) {
    this.initialiseSymbol(symbol);
    this.symbolSnapshot[symbol].estimatedFairPrice = price;
  }

  handleMarketPrice(symbol: string, price: number): Side {
    this.initialiseSymbol(symbol);
    this.symbolSnapshot[symbol].marketPrice = price;
    const {tradeType, tradePrice, qty} = this.computeTradeType(symbol);
    this.updateTradeAudit(symbol, tradeType, tradePrice, qty);
    return tradeType;
  }

  private updateTradeAudit(
    symbol: string,
    tradeType: Side,
    tradePrice: number,
    qty: number
  ) {
    if (tradeType === Side.Invalid) {
      return;
    }
    this.symbolSnapshot[symbol].pendingOrders[tradeType][tradePrice] =
      (this.symbolSnapshot[symbol].pendingOrders[tradeType][tradePrice] ?? 0) +
      qty;
  }
  private computePendingOrdersBalance(symbol: string) {
    if (this.symbolSnapshot[symbol]?.pendingOrders === undefined) {
      return 0;
    }
    let totalPendingBalance = 0;
    for (const tradedPrice in this.symbolSnapshot[symbol].pendingOrders[
      Side.Buy
    ]) {
      totalPendingBalance +=
        parseFloat(tradedPrice) *
        this.symbolSnapshot[symbol].pendingOrders[Side.Buy][tradedPrice];
    }
    return totalPendingBalance;
  }

  private computePendingSellUnits(symbol: string) {
    if (this.symbolSnapshot[symbol]?.pendingOrders === undefined) {
      return 0;
    }
    let totalPendingSellUnits = 0;
    for (const tradedPrice in this.symbolSnapshot[symbol].pendingOrders[
      Side.Sell
    ]) {
      totalPendingSellUnits +=
        this.symbolSnapshot[symbol].pendingOrders[Side.Sell][tradedPrice];
    }
    return totalPendingSellUnits;
  }

  computeTradeType(symbol: string) {
    let tradeType = Side.Invalid;
    const currentSymbolSnapshot = this.symbolSnapshot[symbol];
    if (
      currentSymbolSnapshot.estimatedFairPrice === undefined ||
      currentSymbolSnapshot.marketPrice === undefined ||
      currentSymbolSnapshot.marketPrice ===
        currentSymbolSnapshot.estimatedFairPrice
    ) {
      tradeType = Side.Invalid;
    } else if (
      currentSymbolSnapshot.marketPrice <
      currentSymbolSnapshot.estimatedFairPrice
    ) {
      const remainderBalance =
        this.availableAmount -
        this.computePendingOrdersBalance(symbol) -
        currentSymbolSnapshot.marketPrice;
      if (remainderBalance < 0) {
        tradeType = Side.Invalid;
      } else {
        tradeType = Side.Buy;
      }
    } else if (
      currentSymbolSnapshot.marketPrice >
      currentSymbolSnapshot.estimatedFairPrice
    ) {
      if (
        currentSymbolSnapshot.purchasedUnits !== undefined &&
        currentSymbolSnapshot.purchasedUnits -
          this.computePendingSellUnits(symbol) >
          0
      ) {
        tradeType = Side.Sell;
      } else {
        tradeType = Side.Invalid;
      }
    }
    console.log("Computed trade type - ", tradeType);
    return {
      tradeType,
      qty: 1,
      tradePrice: currentSymbolSnapshot.marketPrice ?? 0,
    };
  }

  handleFill(
    symbol: string,
    qty: number,
    originalLimitPrice: number,
    fillPrice: number
  ) {
    const tradeType = qty < 0 ? Side.Sell : Side.Buy;
    const updatedQty = (this.symbolSnapshot[symbol].pendingOrders[tradeType][
      originalLimitPrice
    ] -= Math.abs(qty));
    if (updatedQty < 0) {
      throw new Error(
        `Incorrect fill event received. Not enough pending orders to fullfill - Params - Symbol ${symbol}, Qty ${qty}, Limit Price ${originalLimitPrice}, Fill Price ${fillPrice}`
      );
    }
    this.symbolSnapshot[symbol].pendingOrders[tradeType][originalLimitPrice] =
      updatedQty;

    const updatedPurchaseUnits =
      this.symbolSnapshot[symbol].purchasedUnits + qty;
    if (updatedPurchaseUnits < 0) {
      throw new Error(
        `Incorrect fill event received. Cannot sell more than existing total units of share - Params - Symbol ${symbol}, Qty ${qty}, Limit Price ${originalLimitPrice}, Fill Price ${fillPrice}`
      );
    }
    this.symbolSnapshot[symbol].purchasedUnits = updatedPurchaseUnits;
    const updatedAvailableAmount = this.availableAmount + fillPrice * -1 * qty;
    if (updatedAvailableAmount < 0) {
      throw new Error(
        `Incorrect fill event received. Available amount cannot reduce beyond 0 - Params - Symbol ${symbol}, Qty ${qty}, Limit Price ${originalLimitPrice}, Fill Price ${fillPrice}`
      );
    }
    this.availableAmount = updatedAvailableAmount;

    const {
      tradeType: newTradeType,
      tradePrice: newTradePrice,
      qty: newQty,
    } = this.computeTradeType(symbol);
    this.updateTradeAudit(symbol, newTradeType, newTradePrice, newQty);
    return newTradeType;
  }

  notifyReject(symbol: string, qty: number, originalLimitPrice: number) {
    const tradeType = qty < 0 ? Side.Sell : Side.Buy;
    const updatedQty = (this.symbolSnapshot[symbol].pendingOrders[tradeType][
      originalLimitPrice
    ] -= Math.abs(qty));
    if (updatedQty < 0) {
      throw new Error(
        `Incorrect Reject event received. Not enough pending orders to fullfill - Params - Symbol ${symbol}, Qty ${qty}, Limit Price ${originalLimitPrice}`
      );
    }
    this.symbolSnapshot[symbol].pendingOrders[tradeType][originalLimitPrice] =
      updatedQty;
  }

  getAvailableCash() {
    return this.availableAmount;
  }

  getSymbolSnapShot(symbol: string) {
    return this.symbolSnapshot[symbol];
  }
}
